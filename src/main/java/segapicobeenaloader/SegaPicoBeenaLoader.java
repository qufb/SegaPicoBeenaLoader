/* ###
 * IP: GHIDRA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package segapicobeenaloader;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import docking.widgets.OptionDialog;
import docking.widgets.filechooser.GhidraFileChooser;
import ghidra.app.cmd.data.CreateArrayCmd;
import ghidra.app.cmd.data.CreateDataCmd;
import ghidra.app.cmd.disassemble.ArmDisassembleCommand;
import ghidra.app.util.Option;
import ghidra.app.util.bin.BinaryReader;
import ghidra.app.util.bin.ByteProvider;
import ghidra.app.util.importer.MessageLog;
import ghidra.app.util.opinion.AbstractLibrarySupportLoader;
import ghidra.app.util.opinion.LoadSpec;
import ghidra.program.flatapi.FlatProgramAPI;
import ghidra.program.model.address.Address;
import ghidra.program.model.data.ByteDataType;
import ghidra.program.model.data.DWordDataType;
import ghidra.program.model.data.DataType;
import ghidra.program.model.data.DataUtilities;
import ghidra.program.model.data.DataUtilities.ClearDataMode;
import ghidra.program.model.data.PointerDataType;
import ghidra.program.model.data.WordDataType;
import ghidra.program.model.lang.AddressLabelInfo;
import ghidra.program.model.lang.LanguageCompilerSpecPair;
import ghidra.program.model.listing.Instruction;
import ghidra.program.model.listing.Program;
import ghidra.program.model.mem.Memory;
import ghidra.program.model.mem.MemoryBlock;
import ghidra.program.model.symbol.SourceType;
import ghidra.program.model.util.CodeUnitInsertionException;
import ghidra.util.exception.CancelledException;
import ghidra.util.exception.InvalidInputException;
import ghidra.util.task.TaskMonitor;

/**
 * TODO: Provide class-level documentation that describes what this loader does.
 */
public class SegaPicoBeenaLoader extends AbstractLibrarySupportLoader {

	private GameHeader header;

	@Override
	public String getName() {
		return "Sega Pico Beena Loader";
	}

	@Override
	public Collection<LoadSpec> findSupportedLoadSpecs(ByteProvider provider) throws IOException {
		List<LoadSpec> loadSpecs = new ArrayList<>();

		// FIXME: Support word-swapped ROMs
		BinaryReader reader = new BinaryReader(provider, false);
		if (reader.readAsciiString(0x20, 9).equals("edinburgh")) {
			loadSpecs.add(new LoadSpec(this, 0, new LanguageCompilerSpecPair("ARM:BE:32:v4t", "default"), true));
		}

		return loadSpecs;
	}

	@Override
	protected void load(ByteProvider provider,
			LoadSpec loadSpec,
			List<Option> options,
			Program program,
			TaskMonitor monitor,
			MessageLog log) throws CancelledException, IOException {
		BinaryReader reader = new BinaryReader(provider, false);
		FlatProgramAPI fpa = new FlatProgramAPI(program, monitor);

		header = new GameHeader(reader);

        long baseAddress = header.getBaseAddress0xd4();
        if (baseAddress != 0xFFFFFFFFL) {
            // Only set in CarBeena ROMs?
            baseAddress = baseAddress & 0xffff0000L;
            log.appendMsg(String.format("Using field 0xd4 as baseAddress=%d...", baseAddress));
        } else {
            baseAddress = header.getBaseAddress();
            if (baseAddress < 0 || baseAddress > 0xA0000000L) {
                log.appendMsg(String.format("Invalid baseAddress=%d, assuming header @ 0x80000000...", baseAddress));
                baseAddress = 0x80000000L;
            }
        }

		InputStream romStream = provider.getInputStream(0);
		InputStream romMaskStream = null;
		InputStream romFlashStream = null;
        if (baseAddress == 0xA0000000L) {
            romFlashStream = romStream;
        } else {
            romMaskStream = romStream;
        }

		InputStream biosStream = null;
		int choice = OptionDialog.showOptionNoCancelDialog(
			null,
			"BIOS mapping",
			"Load BIOS file?",
			"Yes",
			"No (Just create empty mapping)",
			OptionDialog.QUESTION_MESSAGE
		);
		if (choice == OptionDialog.OPTION_ONE) {
			GhidraFileChooser chooser = new GhidraFileChooser(null);
			chooser.setTitle("Open BIOS file");
			File file = chooser.getSelectedFile(true);
			if (file != null) {
				biosStream = new FileInputStream(file);
			}
		}

		createSegment(fpa, biosStream, "BIOS_ROM", 0x00000000L, 0x20000L, true, false, true, false, log);
		createSegment(fpa, null,       "BIOS_RAM", 0x20000000L, 0x4000L, true, true, false, true, log);

		createSegment(fpa, null,           "VIDEO",     0x40000000L, 0x40000L, true, true, false, true, log);
		createSegment(fpa, null,           "IO",        0x50000000L, 0x40000L, true, true, false, true, log);
		createSegment(fpa, null,           "RTC",       0x60000000L, 0x40000L, true, true, false, true, log);
		createSegment(fpa, null,           "MIDI",      0x70000000L, 0x40000L, true, true, false, true, log);
        if (romMaskStream != null) {
            createSegment(fpa, romMaskStream,  "ROM_MASK",  0x80000000L, Math.min(romStream.available(), 0x800000L), true, false, true, false, log);
        }
        if (romFlashStream != null) {
            createSegment(fpa, romFlashStream, "ROM_FLASH", 0xA0000000L, Math.min(romStream.available(), 0x800000L), true, false, true, false, log);
        }
		createSegment(fpa, null,           "RAM",       0xC0000000L, 0x02000000L, true, true, true, true, log);

		createNamedData(fpa, program,  0x40000000L, "VIDEO_STATUS",            DWordDataType.dataType, log);
		createNamedData(fpa, program,  0x40000004L, "VIDEO_PIXEL_CLOCK",       DWordDataType.dataType, log);
		createNamedData(fpa, program,  0x40000010L, "VIDEO_LAYER_CTRL",        DWordDataType.dataType, log);
		createNamedData(fpa, program,  0x40000020L, "VIDEO_BITMAP_FULL_W_H",   DWordDataType.dataType, log);
		createNamedData(fpa, program,  0x40000024L, "VIDEO_BITMAP_OFFSET_X_Y", DWordDataType.dataType, log);
		createNamedData(fpa, program,  0x40000028L, "VIDEO_BITMAP_SCROLL_X_Y", DWordDataType.dataType, log);
		createNamedData(fpa, program,  0x4000002CL, "VIDEO_BITMAP_CLIP_W_H",   DWordDataType.dataType, log);
		createNamedData(fpa, program,  0x40000030L, "VIDEO_FADEOUT_STEP",      DWordDataType.dataType, log);
		createNamedData(fpa, program,  0x40000038L, "VIDEO_SPRITE_DELTA_X_Y",  DWordDataType.dataType, log);
		createNamedArray(fpa, program, 0x40010000L, "VIDEO_TILEMAP_SPRITES",   0x10000 / WordDataType.dataType.getLength(), WordDataType.dataType, log);
		createNamedArray(fpa, program, 0x40020000L, "VIDEO_PALETTE",           0x1000  / WordDataType.dataType.getLength(), WordDataType.dataType, log);
		createNamedArray(fpa, program, 0x40030000L, "VIDEO_TILEMAP_SCROLL_Y",  0x1000  / WordDataType.dataType.getLength(), WordDataType.dataType, log);

		createNamedData(fpa, program, 0x50010000L, "PCM_STATUS", DWordDataType.dataType, log);
		createNamedData(fpa, program, 0x50010004L, "PCM_CTRL", DWordDataType.dataType, log);
		createNamedData(fpa, program, 0x5001000CL, "PCM_DATA_1", DWordDataType.dataType, log);
		createNamedData(fpa, program, 0x50010010L, "PCM_DATA_2", DWordDataType.dataType, log);

		createNamedData(fpa, program, 0x50020000L, "IO_PENS_STATUS", DWordDataType.dataType, log);
		createNamedData(fpa, program, 0x50020008L, "IO_PEN1_DATA_1", DWordDataType.dataType, log);
		createNamedData(fpa, program, 0x5002000CL, "IO_PEN1_DATA_2", DWordDataType.dataType, log);
		createNamedData(fpa, program, 0x50020010L, "IO_PEN1_DATA_3", DWordDataType.dataType, log);
		createNamedData(fpa, program, 0x50020014L, "IO_PEN1_DATA_4", DWordDataType.dataType, log);
		createNamedData(fpa, program, 0x50020018L, "IO_PEN2_DATA_1", DWordDataType.dataType, log);
		createNamedData(fpa, program, 0x5002001CL, "IO_PEN2_DATA_2", DWordDataType.dataType, log);
		createNamedData(fpa, program, 0x50020020L, "IO_PEN2_DATA_3", DWordDataType.dataType, log);
		createNamedData(fpa, program, 0x50020024L, "IO_PEN2_DATA_4", DWordDataType.dataType, log);
		createNamedData(fpa, program, 0x50020028L, "IO_PENS_CTRL", DWordDataType.dataType, log);
		createNamedData(fpa, program, 0x5002002CL, "IO_PAGES_1_4", DWordDataType.dataType, log);
		createNamedData(fpa, program, 0x50020030L, "IO_PAGES_5_6", DWordDataType.dataType, log);
		createNamedData(fpa, program, 0x50020034L, "IO_PAD_BUTTONS", DWordDataType.dataType, log);
		createNamedData(fpa, program, 0x50020038L, "IO_POWER_STATUS", DWordDataType.dataType, log);
		createNamedData(fpa, program, 0x5002003CL, "IO_POWER_CTRL", DWordDataType.dataType, log);

		createNamedData(fpa, program, 0x50030008L, "IO_EXPANSION_CTRL", DWordDataType.dataType, log);
		createNamedData(fpa, program, 0x5003000CL, "IO_EXPANSION_STATUS", DWordDataType.dataType, log);
		createNamedData(fpa, program, 0x500300BCL, "IO_MEMCACHE_STATUS", DWordDataType.dataType, log);
		createNamedData(fpa, program, 0x500300C0L, "IO_MEMCACHE_CTRL", DWordDataType.dataType, log);

		createNamedData(fpa, program, 0x60000000L, "RTC_SECOND", DWordDataType.dataType, log);
		createNamedData(fpa, program, 0x60000004L, "RTC_MINUTE", DWordDataType.dataType, log);
		createNamedData(fpa, program, 0x60000008L, "RTC_HOUR", DWordDataType.dataType, log);
		createNamedData(fpa, program, 0x6000000CL, "RTC_MDAY", DWordDataType.dataType, log);
		createNamedData(fpa, program, 0x60000010L, "RTC_WEEKDAY", DWordDataType.dataType, log);
		createNamedData(fpa, program, 0x60000014L, "RTC_MONTH", DWordDataType.dataType, log);
		createNamedData(fpa, program, 0x60000018L, "RTC_YEAR", DWordDataType.dataType, log);
		createNamedData(fpa, program, 0x6000001CL, "RTC_CTRL", DWordDataType.dataType, log);

		createNamedData(fpa, program, 0x70000000L, "MIDI_STATUS", DWordDataType.dataType, log);
		createNamedData(fpa, program, 0x70000004L, "MIDI_CTRL", DWordDataType.dataType, log);
		createNamedData(fpa, program, 0x70000008L, "MIDI_DATA", DWordDataType.dataType, log);
		createNamedData(fpa, program, 0x7000000CL, "MIDI_MESSAGE", DWordDataType.dataType, log);
		createNamedData(fpa, program, 0x70000018L, "MIDI_DIVISION", DWordDataType.dataType, log);
		createNamedData(fpa, program, 0x7000001CL, "MIDI_PLAYBACK", DWordDataType.dataType, log);

		createNamedArray(fpa, program, 0xC0000000L, "VIDEO_BITMAP",           0xC6000 / WordDataType.dataType.getLength(), WordDataType.dataType, log);
		createNamedArray(fpa, program, 0xC00C6000L, "VIDEO_TILEMAP_SCROLL_X",   0x900 / WordDataType.dataType.getLength(), WordDataType.dataType, log);
		createNamedArray(fpa, program, 0xC00C8000L, "VIDEO_TILEMAP_FG",        0x2000 / WordDataType.dataType.getLength(), WordDataType.dataType, log);
		createNamedArray(fpa, program, 0xC00CA000L, "VIDEO_TILEMAP_BG",        0x2000 / WordDataType.dataType.getLength(), WordDataType.dataType, log);
		createNamedArray(fpa, program, 0xC0100000L, "VIDEO_TILES_LAYERS",     0x80000, ByteDataType.dataType, log);
		createNamedArray(fpa, program, 0xC0180000L, "VIDEO_TILES_SPRITES",    0x80000, ByteDataType.dataType, log);

		markHeader(baseAddress, program, fpa, log);

		// ARM interrupt vectors
		for (int i = 0; i < 0x20; i += 4) {
			new ArmDisassembleCommand(fpa.toAddr(i), null, false).applyTo(program);
		}

		// BIOS header
		try {
			fpa.createAsciiString(fpa.toAddr(0x20), 0x10);
			fpa.createAsciiString(fpa.toAddr(0x30), 0x10);
		} catch (CodeUnitInsertionException e) {
			log.appendException(e);
		}

		// BIOS address table
		for (int i = 0x40; i < 0x100; i += 4) {
			new CreateDataCmd(fpa.toAddr(i), new PointerDataType()).applyTo(program);
		}
		Map <Long, String> mappings = new HashMap<>();
		mappings.put(0x40L, "ResetHandler");
		mappings.put(0x44L, "UndefHandler");
		mappings.put(0x48L, "SVCHandler");
		mappings.put(0x4CL, "PAbortHandler");
		mappings.put(0x50L, "DAbortHandler");
		mappings.put(0x54L, "IRQHandler");
		mappings.put(0x58L, "FIQHandler");
		mappings.forEach((address, name) -> {
			Address entry = fpa.toAddr(address);
			try {
				fpa.createLabel(entry, name, true);
			} catch (Exception e) {
				log.appendException(e);
			}
		});
		for (int i = 0x100; i < 0x200; i += 4) {
			new CreateDataCmd(fpa.toAddr(i), new DWordDataType()).applyTo(program);
		}

		// BIOS function branches
		final boolean isBiosLoaded = biosStream != null;
		mappings = new HashMap<>();
		mappings.put(0x200L, "bios_strncpy_u64"); // same loop unrolled version as 0x20c?
		mappings.put(0x204L, "bios_bzero");
		mappings.put(0x208L, "bios_memset");
		mappings.put(0x20CL, "bios_strncpy_u64_2");
		mappings.put(0x210L, "bios_strncpy");
		mappings.put(0x214L, "bios_decompress");
		mappings.put(0x218L, "bios_int_div");
		mappings.put(0x21CL, "bios_int_div2");
		mappings.put(0x220L, "bios_mod");
		mappings.put(0x224L, "bios_mod2");
		mappings.put(0x228L, "bios_wait_n"); // wait_n: subs r0,r0,#1; bne wait_n;
		mappings.put(0x22CL, "bios_store"); // str r1,[r0,#0]
		mappings.put(0x230L, "bios_unk_0x230");
		mappings.put(0x234L, "bios_unk_0x234");
		mappings.put(0x238L, "bios_unk_0x238");
		mappings.put(0x23CL, "bios_strncpy_xor_neg_u16"); // assignments xor'd with 0x8000
		mappings.put(0x240L, "bios_strncpy_or_neg_u16"); // assignments or'd with 0x8000
		mappings.put(0x244L, "bios_neg_rep_u16"); // repeats same assignments in loop
		mappings.put(0x248L, "bios_neg_u16");
		mappings.put(0x24CL, "bios_replace_match_u16");
		mappings.put(0x250L, "bios_memset_u16");
		mappings.put(0x2ACL, "bios_undef_handler");
		mappings.put(0x2B4L, "bios_svc_handler");
		mappings.put(0x2BCL, "bios_pabort_handler");
		mappings.put(0x2C4L, "bios_dabort_handler");
		mappings.put(0x2CCL, "bios_irq_handler");
		mappings.put(0x2D4L, "bios_fiq_handler");
		mappings.forEach((address, name) -> {
			try {
				fpa.createLabel(fpa.toAddr(address), name, true);
				if (isBiosLoaded) {
					new ArmDisassembleCommand(fpa.toAddr(address), null, false).applyTo(program);
					Instruction ins = fpa.getInstructionAt(fpa.toAddr(address));
					if (ins != null && ins.getMnemonicString().equalsIgnoreCase("b")) {
						Address biosFuncAddress = fpa.toAddr(Long.decode(ins.getDefaultOperandRepresentation(0)));
						fpa.createFunction(biosFuncAddress, name);
					}
				}
			} catch (Exception e) {
				log.appendException(e);
			}
		});

		// Game ROM entry point
		Address entry = fpa.toAddr(baseAddress);
		try {
			fpa.createLabel(entry, "reset", true);
		} catch (Exception e) {
			log.appendException(e);
		}
		new ArmDisassembleCommand(entry, null, false).applyTo(program);
		Instruction ins = fpa.getInstructionAt(entry);
		if (ins != null && ins.getMnemonicString().equalsIgnoreCase("b")) {
			fpa.createFunction(fpa.toAddr(Long.decode(ins.getDefaultOperandRepresentation(0))), "reset");
		}
		fpa.addEntryPoint(entry);

		/**
		 * Unknown handler, seems to jump to alternative ROM mapping (maybe flash memory)?
		 * BIOS doesn't appear to reference it directly...
		 *
		 * Example:
		 *
		 * 800000f0 e3 a0 04 81   mov    r0,#0x81000000
		 * 800000f4 e3 a0 c4 a1   mov    r12,#0xa1000000
		 * 800000f8 e1 50 00 0f   cmp    r0,pc
		 * 800000fc b2 4c f9 01   sublt  pc=>LAB_a0ffc000,r12,#0x4000
		 */
		new ArmDisassembleCommand(fpa.toAddr(baseAddress + 0xf0), null, false).applyTo(program);

		// Always use language defined labels, regardless of APPLY_LABELS_OPTION_NAME...
		List<AddressLabelInfo> labels = loadSpec.getLanguageCompilerSpec().getLanguage().getDefaultSymbols();
		for (AddressLabelInfo info : labels) {
			try {
				// ...but only ARM interrupt vectors.
				final long offset = info.getAddress().getUnsignedOffset();
				if (offset > 0x20) {
					continue;
				}
				program.getSymbolTable().createLabel(info.getAddress(), info.getLabel(), SourceType.IMPORTED);
			} catch (InvalidInputException e) {
				log.appendException(e);
			}
		}

		monitor.setMessage(String.format("%s : Loading done", getName()));
	}

	private void createSegment(FlatProgramAPI fpa,
			InputStream stream,
			String name,
			long address,
			long size,
			boolean read,
			boolean write,
			boolean execute,
			boolean volatil,
			MessageLog log) {
		MemoryBlock block;
		try {
			block = fpa.createMemoryBlock(name, fpa.toAddr(address), stream, size, false);
			block.setRead(read);
			block.setWrite(write);
			block.setExecute(execute);
			block.setVolatile(volatil);
		} catch (Exception e) {
			log.appendException(e);
		}
	}

	private void createNamedData(FlatProgramAPI fpa,
			Program program,
			long address,
			String name,
			DataType type,
			MessageLog log) {
		try {
			if (type.equals(ByteDataType.dataType)) {
				fpa.createByte(fpa.toAddr(address));
			} else if (type.equals(WordDataType.dataType)) {
				fpa.createWord(fpa.toAddr(address));
			} else if (type.equals(DWordDataType.dataType)) {
				fpa.createDWord(fpa.toAddr(address));
			}
			program.getSymbolTable().createLabel(fpa.toAddr(address), name, SourceType.IMPORTED);
		} catch (Exception e) {
			log.appendException(e);
		}
	}

	private void createNamedArray(FlatProgramAPI fpa,
			Program program,
			long address,
			String name,
			int numElements,
			DataType type,
			MessageLog log) {
		try {
			CreateArrayCmd arrayCmd = new CreateArrayCmd(fpa.toAddr(address), numElements, type, type.getLength());
			arrayCmd.applyTo(program);
			program.getSymbolTable().createLabel(fpa.toAddr(address), name, SourceType.IMPORTED);
		} catch (InvalidInputException e) {
			log.appendException(e);
		}
	}

	private void createMirrorSegment(Memory memory,
			FlatProgramAPI fpa,
			String name,
			long base,
			long new_addr,
			long size,
			MessageLog log) {
		MemoryBlock block;
		Address baseAddress = fpa.toAddr(base);
		try {
			block = memory.createByteMappedBlock(name, fpa.toAddr(new_addr), baseAddress, size, false);

			MemoryBlock baseBlock = memory.getBlock(baseAddress);
			block.setRead(baseBlock.isRead());
			block.setWrite(baseBlock.isWrite());
			block.setExecute(baseBlock.isExecute());
			block.setVolatile(baseBlock.isVolatile());
		} catch (Exception e) {
			log.appendException(e);
		}
	}

	private void markHeader(long baseAddress, Program program, FlatProgramAPI fpa, MessageLog log) {
		try {
			DataUtilities.createData(program, fpa.toAddr(baseAddress + 0x8), header.toDataType(), -1, false, ClearDataMode.CLEAR_ALL_UNDEFINED_CONFLICT_DATA);
		} catch (CodeUnitInsertionException e) {
			log.appendException(e);
		}
	}
}
