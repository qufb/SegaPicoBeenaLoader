package segapicobeenaloader;

import java.io.IOException;

import ghidra.app.util.bin.BinaryReader;
import ghidra.app.util.bin.StructConverter;
import ghidra.program.model.data.DataType;
import ghidra.program.model.data.Structure;
import ghidra.program.model.data.StructureDataType;

class GameHeader implements StructConverter {
	private long sp;
	private short chksumVar1;
	private short chksumVar2;
	private long flashSize;
	private long maskSize;
	private byte[] chipCodeName;
	private byte[] productId;
	private byte isMainRun;
	private byte isHwInitRun;
	private short unk0x52;
	private long val0x60020000;
	private long val0x60020004;
	private long unk0x5c;
	private byte[] productName;
	private byte[] chipCodeName2;
	private byte[] companyName;
	private byte[] appDate;
	private byte[] libDate;
	private long chksum;
	private byte[] unk0x10;
	private byte[] unk0x1c;
	private byte[] unk0x30;
	private byte[] unk0x34;
	private byte[] unk0x38;
	private byte[] unk0x3c;
	private byte[] unkDate;
	private long baseAddress0xd4;
	private long unk0xd8;
	private long baseAddress;

	GameHeader(BinaryReader reader) throws IOException {

		if (reader.length() < 0x100) {
			return;
		}

		reader.setPointerIndex(0x08);
		sp = reader.readNextUnsignedInt();
		chksumVar1 = (short) reader.readNextUnsignedShort();
		chksumVar2 = (short) reader.readNextUnsignedShort();

		unk0x10 = reader.readNextByteArray(0x4);

		flashSize = reader.readNextUnsignedInt();
		maskSize = reader.readNextUnsignedInt();

		unk0x1c = reader.readNextByteArray(0x4);

		chipCodeName = reader.readNextByteArray(0x10);

		unk0x30 = reader.readNextByteArray(0x4);
		unk0x34 = reader.readNextByteArray(0x4);
		unk0x38 = reader.readNextByteArray(0x4);
		unk0x3c = reader.readNextByteArray(0x4);

		productId = reader.readNextByteArray(0x10);
		isMainRun = reader.readNextByte();
		isHwInitRun = reader.readNextByte();

		unk0x52 = (short) reader.readNextUnsignedShort();

		val0x60020000 = reader.readNextUnsignedInt();
		val0x60020004 = reader.readNextUnsignedInt();

		unk0x5c = reader.readNextUnsignedInt();

		productName = reader.readNextByteArray(0x20);
		chipCodeName2 = reader.readNextByteArray(0x10);
		companyName = reader.readNextByteArray(0x10);
		appDate = reader.readNextByteArray(0x10);
		libDate = reader.readNextByteArray(0x10);

		unkDate = reader.readNextByteArray(0x10);

		chksum = reader.readNextUnsignedInt();

		baseAddress0xd4 = reader.readNextUnsignedInt();
		unk0xd8 = reader.readNextUnsignedInt();
		baseAddress = reader.readNextUnsignedInt();
	}

	@Override
	public DataType toDataType() {
		Structure s = new StructureDataType("GameHeader", 0);

		s.add(POINTER, 0x04, "SP", null);
		s.add(WORD, 0x2, "ChksumVar1", null);
		s.add(WORD, 0x2, "ChksumVar2", null);

		s.add(DWORD, 0x4, "Unk0x10", null);

		s.add(DWORD, 0x04, "FlashSize", null);
		s.add(DWORD, 0x04, "MaskSize", null);

		s.add(DWORD, 0x4, "Unk0x1c", null);

		s.add(STRING, 0x10, "ChipCodeName", null);

		s.add(DWORD, 0x4, "Unk0x30", null);
		s.add(DWORD, 0x4, "Unk0x34", null);
		s.add(DWORD, 0x4, "Unk0x38", null);
		s.add(DWORD, 0x4, "Unk0x3c", null);

		s.add(STRING, 0x10, "ProductId", null);
		s.add(BYTE, 0x01, "IsMainRun", null);
		s.add(BYTE, 0x01, "IsHwInitRun", null);

		s.add(WORD, 0x02, "Unk0x52", null);

		s.add(DWORD, 0x04, "Val0x60020000", null);
		s.add(DWORD, 0x04, "Val0x60020004", null);

		s.add(DWORD, 0x04, "Unk0x5C", null);

		s.add(STRING, 0x20, "ProductName", null);
		s.add(STRING, 0x10, "ChipCodeName2", null);
		s.add(STRING, 0x10, "CompanyName", null);
		s.add(STRING, 0x10, "AppDate", null);
		s.add(STRING, 0x10, "LibDate", null);
		s.add(STRING, 0x10, "UnkDate", null);
		s.add(DWORD, 0x04, "Chksum", null);

		s.add(DWORD, 0x04, "baseAddress0xD4", null);
		s.add(DWORD, 0x04, "Unk0xD8", null);
		s.add(DWORD, 0x04, "BaseAddress", null);

		return s;
	}

	public long getSp() {
		return sp;
	}

	public short getChksumVar1() {
		return chksumVar1;
	}

	public short getChksumVar2() {
		return chksumVar2;
	}

	public long getFlashSize() {
		return flashSize;
	}

	public long getMaskSize() {
		return maskSize;
	}

	public byte[] getChipCodeName() {
		return chipCodeName;
	}

	public byte[] getProductId() {
		return productId;
	}

	public byte getIsMainRun() {
		return isMainRun;
	}

	public byte getIsHwInitRun() {
		return isHwInitRun;
	}

	public long getVal0x60020000() {
		return val0x60020000;
	}

	public long getVal0x60020004() {
		return val0x60020004;
	}

	public byte[] getProductName() {
		return productName;
	}

	public byte[] getChipCodeName2() {
		return chipCodeName2;
	}

	public byte[] getCompanyName() {
		return companyName;
	}

	public byte[] getAppDate() {
		return appDate;
	}

	public byte[] getLibDate() {
		return libDate;
	}

	public long getChksum() {
		return chksum;
	}

	public short getUnk0x52() {
		return unk0x52;
	}

	public long getUnk0x5c() {
		return unk0x5c;
	}

	public byte[] getUnk0x10() {
		return unk0x10;
	}

	public byte[] getUnk0x1c() {
		return unk0x1c;
	}

	public byte[] getUnk0x30() {
		return unk0x30;
	}

	public byte[] getUnk0x34() {
		return unk0x34;
	}

	public byte[] getUnk0x38() {
		return unk0x38;
	}

	public byte[] getUnk0x3c() {
		return unk0x3c;
	}

	public byte[] getUnkDate() {
		return unkDate;
	}

	public long getBaseAddress0xd4() {
		return baseAddress0xd4;
	}

	public long getUnk0xd8() {
		return unk0xd8;
	}

	public long getBaseAddress() {
		return baseAddress;
	}
}
